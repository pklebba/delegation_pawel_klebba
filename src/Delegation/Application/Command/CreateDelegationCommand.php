<?php

namespace App\Delegation\Application\Command;

use App\Delegation\Domain\CountryEnum;
use App\Shared\Domain\Command\CommandInterface;

class CreateDelegationCommand implements CommandInterface
{
    public function __construct(
        public readonly string $id,
        public readonly string $employerId,
        public readonly string $startDate,
        public readonly string $endDate,
        public readonly CountryEnum $country,
    ) {
    }
}
