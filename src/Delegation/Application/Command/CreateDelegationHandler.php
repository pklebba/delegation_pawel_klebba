<?php

namespace App\Delegation\Application\Command;

use App\Delegation\Domain\CreateDelegationException;
use App\Delegation\Domain\Delegation;
use App\Delegation\Domain\DelegationRepositoryInterface;
use App\Employer\Domain\EmployerRepositoryInterface;
use App\Shared\Domain\Command\CommandHandlerInterface;

/**
 * @throws CreateDelegationException
 */
class CreateDelegationHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly DelegationRepositoryInterface $delegationRepository,
        private readonly EmployerRepositoryInterface $employerRepository,
    ) {
    }

    public function __invoke(CreateDelegationCommand $cmd)
    {
        $startDate = new \DateTimeImmutable($cmd->startDate);
        $endDate = new \DateTimeImmutable($cmd->endDate);

        $overlapDelegations = $this->delegationRepository->findBetweenTimeRangeByEmployerId($startDate, $endDate, $cmd->employerId);
        if (!empty($overlapDelegations)) {
            throw new CreateDelegationException('There are overlapping delegations.');
        }

        $delegation = new Delegation(
            $cmd->id,
            $this->employerRepository->findById($cmd->employerId),
            $startDate,
            $endDate,
            $cmd->country->value,
        );
        $this->delegationRepository->add($delegation);
    }
}
