<?php

namespace App\Delegation\Application\Query;

use App\Delegation\Domain\CountryEnum;
use App\Delegation\Domain\Delegation;
use App\Delegation\Domain\DelegationRepositoryInterface;
use App\Delegation\Domain\DelegationTimeRange;
use App\Delegation\Domain\DelegationView;
use App\Delegation\Domain\Diet\DietCounter;
use App\Shared\Domain\Query\QueryHandlerInterface;

class ListOfEmployerDelegationsHandler implements QueryHandlerInterface
{
    public function __construct(private readonly DelegationRepositoryInterface $repository)
    {
    }

    public function __invoke(ListOfEmployerDelegationsQuery $query)
    {
        $delegations = $this->repository->findAllByEmployerId($query->employerId);
        $delegationsView = array_map(fn (Delegation $delegation) => $this->makeDelegationViewFromEntity($delegation), $delegations);

        return new ListOfEmployerDelegationsResponse($delegationsView);
    }

    private function makeDelegationViewFromEntity(Delegation $delegation): DelegationView
    {
        /** @var CountryEnum $country */
        $country = CountryEnum::from($delegation->country);
        $delegationTimeRange = new DelegationTimeRange($delegation->startDate, $delegation->endDate);
        $dietCounter = new DietCounter($delegationTimeRange, $country);

        return new DelegationView(
            $delegationTimeRange->getFirstDay()->getIso8601Format(),
            $delegationTimeRange->getLastDay()->getIso8601Format(),
            $delegation->country,
            $dietCounter->getAmountForDelegation(),
            $country->currency(),
        );
    }
}
