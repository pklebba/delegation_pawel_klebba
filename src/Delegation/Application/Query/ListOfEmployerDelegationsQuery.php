<?php

namespace App\Delegation\Application\Query;

use App\Shared\Domain\Query\QueryInterface;

class ListOfEmployerDelegationsQuery implements QueryInterface
{
    public function __construct(
        public readonly string $employerId,
    ) {
    }
}
