<?php

namespace App\Delegation\Application\Query;

use App\Delegation\Domain\DelegationView;
use App\Shared\Domain\Query\QueryResponseInterface;

class ListOfEmployerDelegationsResponse implements QueryResponseInterface
{
    /**
     * @param DelegationView[] $delegations
     */
    public function __construct(public readonly array $delegations)
    {
    }
}
