<?php

namespace App\Delegation\Infrastructure;

use App\Delegation\Domain\Delegation;
use App\Delegation\Domain\DelegationRepositoryInterface;
use App\Employer\Domain\Employer;
use DateTimeInterface as DateTimeInterfaceAlias;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

final class DoctrineOrmDelegationRepository implements DelegationRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
    }

    /**
     * @return Delegation[]
     */
    public function findAllByEmployerId(string $id): array
    {
        return $this->em->getRepository(Delegation::class)->findBy(['employer' => new Employer($id)]);
    }

    public function add(Delegation $delegation): Delegation
    {
        $this->em->persist($delegation);
        $this->em->flush();

        return $delegation;
    }

    /**
     * @return Delegation[]
     */
    public function findBetweenTimeRangeByEmployerId(
        DateTimeInterfaceAlias $startDate,
        DateTimeInterfaceAlias $endDate,
        string $employerId,
    ): array {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('employer', new Employer($employerId)))
            ->andWhere(
                Criteria::expr()->andX(
                    Criteria::expr()->lte('startDate', $endDate),
                    Criteria::expr()->gte('endDate', $startDate),
                ),
            )
            ->orderBy(['endDate' => Criteria::DESC]);

        return $this->em->getRepository(Delegation::class)->matching($criteria)->getValues();
    }
}
