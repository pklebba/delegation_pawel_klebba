<?php

namespace App\Delegation\Domain;

use Carbon\Carbon;
use Carbon\CarbonInterface;

final class DelegationDay
{
    private CarbonInterface $dateAbstract;

    public function __construct(
        public readonly \DateTimeInterface $date,
    ) {
        $this->dateAbstract = new Carbon($this->date);
    }

    public function isWeekend(): bool
    {
        return $this->dateAbstract->isWeekend();
    }

    public function getIso8601Format(): string
    {
        return $this->dateAbstract->toIso8601String();
    }

    public function getStartedHoursInDay(): float
    {
        $beginOfDay = Carbon::create($this->dateAbstract)->setTime(0, 0);

        return ceil($this->dateAbstract->diffInMinutes($beginOfDay) / 60);
    }

    public function getLeftHoursInDay(): float
    {
        $beginOfDay = Carbon::create($this->dateAbstract)->setTime(23, 59);

        return ceil($this->dateAbstract->diffInMinutes($beginOfDay) / 60);
    }
}
