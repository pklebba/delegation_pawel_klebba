<?php

namespace App\Delegation\Domain;

enum CountryEnum: string
{
    case PL = 'PL';
    case DE = 'DE';
    case GB = 'GB';

    public function amount(): int
    {
        return match ($this) {
            CountryEnum::PL => 10,
            CountryEnum::DE => 50,
            CountryEnum::GB => 75,
        };
    }

    public function currency(): string
    {
        return 'PLN';
    }
}
