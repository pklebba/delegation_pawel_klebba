<?php

namespace App\Delegation\Domain;

interface DelegationRepositoryInterface
{
    public function findAllByEmployerId(string $id): array;

    public function add(Delegation $delegation): Delegation;

    public function findBetweenTimeRangeByEmployerId(\DateTimeInterface $startDate, \DateTimeInterface $endDate, string $employerId);
}
