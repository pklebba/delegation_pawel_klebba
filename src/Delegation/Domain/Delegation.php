<?php

namespace App\Delegation\Domain;

use App\Employer\Domain\Employer;

class Delegation
{
    public function __construct(
        public readonly string $id,
        public readonly Employer $employer,
        public readonly \DateTimeInterface $startDate,
        public readonly \DateTimeInterface $endDate,
        public readonly string $country,
    ) {
    }

    public function isOver(): bool
    {
        return $this->endDate < new \DateTimeImmutable();
    }
}
