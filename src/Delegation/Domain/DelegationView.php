<?php

namespace App\Delegation\Domain;

final class DelegationView
{
    public function __construct(
        public readonly string $start,
        public readonly string $end,
        public readonly string $country,
        public readonly string $amount_due,
        public readonly string $currency,
    ) {
    }
}
