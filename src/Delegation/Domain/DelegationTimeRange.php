<?php

namespace App\Delegation\Domain;

use Carbon\Carbon;

final class DelegationTimeRange
{
    private readonly Carbon $fromDate;
    private readonly Carbon $toDate;

    public function __construct(
        \DateTimeInterface $fromDate,
        \DateTimeInterface $toDate,
    ) {
        $this->fromDate = Carbon::create($fromDate);
        $this->toDate = Carbon::create($toDate);

        if ($this->fromDate->gt($this->toDate)) {
            throw new \InvalidArgumentException('Start date cannot be later than end date');
        }
    }

    public function getNumberOfDaysExcludingWeekend(): int
    {
        $weekDays = $this->fromDate->diffInWeekDays($this->toDate);

        if ($this->toDate->isWeekend()) {
            return $weekDays;
        }

        return $weekDays + 1;
    }

    public function getNumberOfWeeks(): int
    {
        return $this->fromDate->diffInWeeks($this->toDate) + 1;
    }

    public function getNumberOfDays(): int
    {
        return $this->fromDate->diffInDays($this->toDate) + 1;
    }

    public function getTotalNumberOfHours(): int
    {
        return $this->fromDate->diffInHours($this->toDate);
    }

    public function getFirstDay(): DelegationDay
    {
        return new DelegationDay($this->fromDate);
    }

    public function getLastDay(): DelegationDay
    {
        return new DelegationDay($this->toDate);
    }

    /**
     * @return DelegationDay[]
     */
    public function getDelegationDays(): array
    {
        $period = [];
        for ($day = 1; $day <= $this->getNumberOfDays(); ++$day) {
            if (1 == $day) {
                $period[] = new DelegationDay($this->fromDate->toDateTimeImmutable());
                continue;
            }

            if ($day == $this->getNumberOfDays()) {
                $period[] = new DelegationDay($this->toDate->toDateTimeImmutable());
                continue;
            }

            $currentFullDay = (new Carbon($this->fromDate))->addDays($day - 1)->setTime(23, 59);
            $period[] = new DelegationDay($currentFullDay);
        }

        return $period;
    }

    public function isOneDay(): bool
    {
        return (new Carbon($this->fromDate))->setTime(0, 0)->getTimestamp() ===
               (new Carbon($this->toDate))->setTime(0, 0)->getTimestamp();
    }
}
