<?php

namespace App\Delegation\Domain\Diet;

use App\Delegation\Domain\CountryEnum;

class DelegationDiet
{
    public function __construct(
        public readonly CountryEnum $country,
        public readonly int $dietFactor = 1,
    ) {
    }

    public function getDietAmount(): int
    {
        return $this->dietFactor * $this->country->amount();
    }
}
