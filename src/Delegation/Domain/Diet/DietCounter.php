<?php

namespace App\Delegation\Domain\Diet;

use App\Delegation\Domain\CountryEnum;
use App\Delegation\Domain\DelegationDay;
use App\Delegation\Domain\DelegationTimeRange;

class DietCounter
{
    public const MINIMUM_HOURS_PER_DAY = 8;

    public function __construct(
        public readonly DelegationTimeRange $delegationTimeRange,
        public readonly CountryEnum $country
    ) {
    }

    public function getAmountForDelegation(): int|float
    {
        $amount = 0;
        $currentDay = 0;
        $totalPayableWeekDayHours = 0;

        $delegationDays = $this->delegationTimeRange->getDelegationDays();
        foreach ($delegationDays as $delegationDay) {
            ++$currentDay;
            $dietFactor = ceil($currentDay / 7);

            $delegationHoursInDay = $this->getDelegationHoursInDay($delegationDay);

            if ($delegationDay->isWeekend()) {
                continue;
            }

            if ($delegationHoursInDay < self::MINIMUM_HOURS_PER_DAY) {
                continue;
            }

            $totalPayableWeekDayHours += $delegationHoursInDay;

            $amount += $dietFactor * $this->country->amount();
        }

        if ($totalPayableWeekDayHours < self::MINIMUM_HOURS_PER_DAY) {
            return 0;
        }

        return $amount;
    }

    private function getDelegationHoursInDay(DelegationDay $delegationDay): int
    {
        if ($this->delegationTimeRange->isOneDay()) {
            return $this->delegationTimeRange->getTotalNumberOfHours();
        }

        if ($this->delegationTimeRange->getFirstDay()->getIso8601Format() === $delegationDay->getIso8601Format()) {
            return $delegationDay->getLeftHoursInDay();
        }

        return $delegationDay->getStartedHoursInDay();
    }
}
