<?php

namespace App\UI\Domain\Delegation;

use App\Delegation\Domain\CountryEnum;
use Symfony\Component\Validator\Constraints as Assert;

class CreateDelegationRequest
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Uuid]
        public readonly string $employerId,
        #[Assert\NotBlank]
        #[Assert\DateTime]
        #[Assert\LessThan(propertyPath: 'endDate')]
        public readonly string $startDate,
        #[Assert\NotBlank]
        #[Assert\DateTime]
        #[Assert\GreaterThan(propertyPath: 'startDate')]
        public readonly string $endDate,
        #[Assert\NotBlank]
        #[Assert\Type(type: CountryEnum::class)]
        public readonly CountryEnum $country,
    ) {
    }
}
