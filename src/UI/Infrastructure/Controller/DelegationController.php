<?php

namespace App\UI\Infrastructure\Controller;

use App\Delegation\Application\Command\CreateDelegationCommand;
use App\Delegation\Application\Query\ListOfEmployerDelegationsQuery;
use App\Delegation\Domain\CreateDelegationException;
use App\Shared\Domain\Command\CommandBusInterface;
use App\Shared\Domain\Query\QueryBusInterface;
use App\UI\Domain\Delegation\CreateDelegationRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

#[Route('/delegation')]
final class DelegationController extends AbstractController
{
    public function __construct(
        private readonly CommandBusInterface $commandBus,
        private readonly QueryBusInterface $queryBus,
    ) {
    }

    #[Route('/employer/{employerId}', methods: ['GET'])]
    public function listOfDelegations(string $employerId): JsonResponse
    {
        $query = new ListOfEmployerDelegationsQuery($employerId);

        $delegations = $this->queryBus->ask($query);

        return new JsonResponse($delegations, Response::HTTP_CREATED);
    }

    #[Route('/', methods: ['POST'])]
    public function createDelegation(
        #[MapRequestPayload] CreateDelegationRequest $delegationRequest,
    ): JsonResponse {
        $id = Uuid::v4();
        $command = new CreateDelegationCommand(
            $id,
            $delegationRequest->employerId,
            $delegationRequest->startDate,
            $delegationRequest->endDate,
            $delegationRequest->country,
        );

        try {
            $this->commandBus->handle($command);
        } catch (CreateDelegationException $createDelegationException) {
            return new JsonResponse([
                'error' => $createDelegationException->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse([
            'id' => $id,
        ], Response::HTTP_CREATED);
    }
}
