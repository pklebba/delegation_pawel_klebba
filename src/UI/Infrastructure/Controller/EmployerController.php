<?php

namespace App\UI\Infrastructure\Controller;

use App\Employer\Application\Command\CreateEmployerCommand;
use App\Shared\Domain\Command\CommandBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

#[Route('/employer')]
final class EmployerController extends AbstractController
{
    public function __construct(
        private readonly CommandBusInterface $commandBus,
    ) {
    }

    #[Route('/', methods: ['POST'])]
    public function createEmployer(): JsonResponse
    {
        $id = Uuid::v4();
        $command = new CreateEmployerCommand($id);

        $this->commandBus->handle($command);

        return new JsonResponse([
            'employer_id' => $id,
        ], Response::HTTP_CREATED);
    }
}
