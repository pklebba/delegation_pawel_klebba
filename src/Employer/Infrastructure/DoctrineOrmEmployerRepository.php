<?php

namespace App\Employer\Infrastructure;

use App\Employer\Domain\Employer;
use App\Employer\Domain\EmployerRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

final class DoctrineOrmEmployerRepository implements EmployerRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
    }

    public function findById(string $id): Employer|null
    {
        return $this->em->find(Employer::class, $id);
    }

    public function add(Employer $employer): Employer
    {
        $this->em->persist($employer);
        $this->em->flush();

        return $employer;
    }
}
