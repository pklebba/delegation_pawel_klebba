<?php

namespace App\Employer\Domain;

class Employer
{
    public function __construct(
        public readonly string $id,
        public readonly array $delegations = [],
    ) {
    }
}
