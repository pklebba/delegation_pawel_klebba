<?php

namespace App\Employer\Domain;

interface EmployerRepositoryInterface
{
    public function findById(string $id): Employer|null;

    public function add(Employer $employer): Employer;
}
