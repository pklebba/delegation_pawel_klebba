<?php

namespace App\Employer\Application\Command;

use App\Employer\Domain\Employer;
use App\Employer\Domain\EmployerRepositoryInterface;
use App\Shared\Domain\Command\CommandHandlerInterface;

final class CreateEmployerHandler implements CommandHandlerInterface
{
    public function __construct(private readonly EmployerRepositoryInterface $employers)
    {
    }

    public function __invoke(CreateEmployerCommand $cmd)
    {
        $employer = new Employer($cmd->id);

        $this->employers->add($employer);
    }
}
