<?php

namespace App\Employer\Application\Command;

use App\Shared\Domain\Command\CommandInterface;

final class CreateEmployerCommand implements CommandInterface
{
    public function __construct(
        public readonly string $id,
    ) {
    }
}
