<?php

namespace App\Shared\Domain\Command;

interface CommandBusInterface
{
    public function handle(CommandInterface $command);
}
