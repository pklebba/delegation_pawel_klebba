<?php

namespace App\Shared\Infrastructure;

use App\Shared\Domain\Query\QueryBusInterface;
use App\Shared\Domain\Query\QueryInterface;
use App\Shared\Domain\Query\QueryResponseInterface;
use Symfony\Component\Messenger\Exception\NoHandlerForMessageException;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;
use Symfony\Component\Messenger\Stamp\HandledStamp;

final class InMemoryQueryBus implements QueryBusInterface
{
    private MessageBus $bus;

    public function __construct(iterable $queryHandlers)
    {
        $this->bus = new MessageBus([
            new HandleMessageMiddleware(
                new HandlersLocator(
                    HandlersBuilder::fromCallables($queryHandlers),
                )
            ),
        ]);
    }

    public function ask(QueryInterface $query): QueryResponseInterface
    {
        try {
            /** @var HandledStamp $stamp */
            $stamp = $this->bus->dispatch($query)->last(HandledStamp::class);

            return $stamp->getResult();
        } catch (NoHandlerForMessageException $e) {
            throw new \InvalidArgumentException(sprintf('Missing or invalid handler for query: %s', $query::class));
        }
    }
}
