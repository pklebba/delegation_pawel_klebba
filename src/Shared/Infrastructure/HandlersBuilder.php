<?php

namespace App\Shared\Infrastructure;

use App\Shared\Domain\Command\CommandHandlerInterface;
use App\Shared\Domain\Query\QueryHandlerInterface;

class HandlersBuilder
{
    /**
     * @throws \ReflectionException
     */
    public static function fromCallables(iterable $callables): array
    {
        $handlers = [];

        foreach ($callables as $callable) {
            $handlers[self::getQCNameFromHandler($callable)] = [$callable];
        }

        return $handlers;
    }

    /**
     * @throws \ReflectionException
     */
    private static function getQCNameFromHandler(CommandHandlerInterface|QueryHandlerInterface $commandQueryHandler): string|null
    {
        $reflection = new \ReflectionObject($commandQueryHandler);

        $method = $reflection->getMethod('__invoke');
        if ($method->getNumberOfParameters() > 0) {
            return $method->getParameters()[0]?->getType()->getName();
        }

        return null;
    }
}
