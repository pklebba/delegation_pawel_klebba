<?php

namespace App\Shared\Infrastructure;

use App\Shared\Domain\Command\CommandBusInterface;
use App\Shared\Domain\Command\CommandInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Exception\NoHandlerForMessageException;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;

final class InMemoryCommandBus implements CommandBusInterface
{
    private MessageBus $messageBus;

    public function __construct(
        iterable $commandHandlers,
    ) {
        $this->messageBus = new MessageBus([
            new HandleMessageMiddleware(
                new HandlersLocator(
                    HandlersBuilder::fromCallables($commandHandlers),
                ),
            ),
        ]);
    }

    /**
     * @throws \Throwable
     */
    public function handle(CommandInterface $command): void
    {
        try {
            $this->messageBus->dispatch($command);
        } catch (NoHandlerForMessageException $e) {
            throw new \InvalidArgumentException(sprintf('Missing or invalid handler for command: %s', $command::class));
        } catch (HandlerFailedException $e) {
            throw $e->getPrevious();
        }
    }
}
