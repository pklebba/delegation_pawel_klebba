<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231116020132 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE delegations (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', employer_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', start_date DATETIME NOT NULL COMMENT \'(DC2Type:datetimetz_immutable)\', end_date DATETIME NOT NULL COMMENT \'(DC2Type:datetimetz_immutable)\', country VARCHAR(255) NOT NULL, INDEX IDX_2824FDAD41CD9E7A (employer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employers (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE delegations ADD CONSTRAINT FK_2824FDAD41CD9E7A FOREIGN KEY (employer_id) REFERENCES employers (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE delegations DROP FOREIGN KEY FK_2824FDAD41CD9E7A');
        $this->addSql('DROP TABLE delegations');
        $this->addSql('DROP TABLE employers');
    }
}
