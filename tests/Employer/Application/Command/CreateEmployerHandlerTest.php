<?php

namespace App\Tests\Employer\Application\Command;

use App\Employer\Application\Command\CreateEmployerCommand;
use App\Employer\Application\Command\CreateEmployerHandler;
use App\Employer\Domain\EmployerRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class CreateEmployerHandlerTest extends TestCase
{
    public function testInvokeHandler()
    {
        $employerRepoStub = $this->createMock(EmployerRepositoryInterface::class);
        $employerRepoStub
            ->expects($this->once())
            ->method('add');

        $delegationHandler = new CreateEmployerHandler($employerRepoStub);
        $command = new CreateEmployerCommand(Uuid::v4());
        $delegationHandler($command);
    }
}
