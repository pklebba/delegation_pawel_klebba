<?php

namespace App\Tests\Delegation\Application\Command;

use App\Delegation\Application\Command\CreateDelegationCommand;
use App\Delegation\Application\Command\CreateDelegationHandler;
use App\Delegation\Domain\CountryEnum;
use App\Delegation\Domain\CreateDelegationException;
use App\Delegation\Domain\Delegation;
use App\Delegation\Domain\DelegationRepositoryInterface;
use App\Employer\Domain\Employer;
use App\Employer\Domain\EmployerRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class CreateDelegationHandlerTest extends TestCase
{
    public function testInvokeHandler()
    {
        $employerId = Uuid::v4();
        $employer = new Employer($employerId);
        $command = new CreateDelegationCommand(
            Uuid::v4(),
            $employerId,
            '2022-11-21 08:00:00',
            '2022-11-21 20:00:00',
            CountryEnum::PL,
        );

        $employerRepoStub = $this->createStub(EmployerRepositoryInterface::class);
        $employerRepoStub
            ->method('findById')
            ->willReturn($employer);

        $delegationRepoMock = $this->createMock(DelegationRepositoryInterface::class);
        $delegationRepoMock
            ->method('findBetweenTimeRangeByEmployerId')
            ->willReturn([]);

        $delegationRepoMock
            ->expects($this->once())
            ->method('add');


        $delegationHandler = new CreateDelegationHandler($delegationRepoMock, $employerRepoStub);
        $delegationHandler($command);
    }

    public function testExceptionWhenDelegationIsOverlapped()
    {
        $this->expectException(CreateDelegationException::class);

        $command = new CreateDelegationCommand(
            Uuid::v4(),
            Uuid::v4(),
            '2022-11-21 08:00:00',
            '2022-11-21 20:00:00',
            CountryEnum::PL,
        );


        $delegationRepoMock = $this->createMock(DelegationRepositoryInterface::class);
        $delegationRepoMock
            ->method('findBetweenTimeRangeByEmployerId')
            ->willReturn(['dummy']);

        $employerRepoStub = $this->createStub(EmployerRepositoryInterface::class);

        $delegationHandler = new CreateDelegationHandler($delegationRepoMock, $employerRepoStub);
        $delegationHandler($command);
    }
}
