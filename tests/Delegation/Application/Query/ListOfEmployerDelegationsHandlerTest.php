<?php

namespace App\Tests\Delegation\Application\Query;

use App\Delegation\Application\Query\ListOfEmployerDelegationsHandler;
use App\Delegation\Application\Query\ListOfEmployerDelegationsQuery;
use App\Delegation\Domain\CountryEnum;
use App\Delegation\Domain\Delegation;
use App\Delegation\Domain\DelegationRepositoryInterface;
use App\Employer\Domain\Employer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

class ListOfEmployerDelegationsHandlerTest extends TestCase
{
    public function testInvokeHandler()
    {
        $employerId = Uuid::v4();
        $employer = new Employer($employerId);
        $startDate = '2023-11-21T08:00:00+00:00';
        $endDate = '2023-11-23T08:00:00+00:00';

        $dummyDelegations = [
            new Delegation(
                Uuid::v4(),
                $employer,
                new \DateTimeImmutable($startDate),
                new \DateTimeImmutable($endDate),
                CountryEnum::PL->value,
            )
        ];

        $delegationRepoMock = $this->createMock(DelegationRepositoryInterface::class);
        $delegationRepoMock
            ->expects($this->once())
            ->method('findAllByEmployerId')
            ->willReturn($dummyDelegations);

        $query = new ListOfEmployerDelegationsQuery($employerId);
        $queryHandler = new ListOfEmployerDelegationsHandler($delegationRepoMock);
        $handlerResult = $queryHandler($query);

        $this->assertEquals($handlerResult->delegations[0]->start, $startDate);
        $this->assertEquals($handlerResult->delegations[0]->end, $endDate);
        $this->assertEquals($handlerResult->delegations[0]->country, CountryEnum::PL->value);
        $this->assertEquals($handlerResult->delegations[0]->currency, CountryEnum::PL->currency());
    }
}
