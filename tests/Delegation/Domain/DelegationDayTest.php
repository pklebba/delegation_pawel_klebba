<?php

namespace App\Tests\Delegation\Domain;

use App\Delegation\Domain\DelegationDay;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class DelegationDayTest extends TestCase
{
    public static function delegationDaysProvider(): array
    {
        return [
            ['2023-11-21T08:00:00+00:00', false, 8],
            ['2023-11-22T08:30:00+00:00', false, 9],
            ['2023-11-23T03:00:00+00:00', false, 3],
            ['2023-11-24T23:00:00+00:00', false, 23],
            ['2023-11-25T13:00:00+00:00', true, 13],
            ['2023-11-26T00:10:00+00:00', true, 1],
        ];
    }
    #[DataProvider('delegationDaysProvider')]
    public function testGetIso8601Format(string $date)
    {
        $delegationDay = new DelegationDay(new \DateTimeImmutable($date));

        $this->assertEquals($date, $delegationDay->getIso8601Format());
    }

    #[DataProvider('delegationDaysProvider')]
    public function testIsWeekend(string $date, bool $expectedResult)
    {
        $delegationDay = new DelegationDay(new \DateTimeImmutable($date));

        $this->assertEquals($expectedResult, $delegationDay->isWeekend());
    }

    #[DataProvider('delegationDaysProvider')]
    public function testHoursInDay(string $date, bool $isWeekend, int $expectedResult)
    {
        $delegationDay = new DelegationDay(new \DateTimeImmutable($date));

        $this->assertEquals($expectedResult, $delegationDay->getStartedHoursInDay());
    }
}
