<?php

namespace App\Tests\Delegation\Domain;

use App\Delegation\Domain\DelegationTimeRange;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class DelegationTimeRangeTest extends TestCase {
    public static function numberOfWeeksProvider()
    {
        return [
            ['2023-01-01', '2023-01-02', 1],
            ['2023-01-01', '2023-01-07', 1],
            ['2023-01-01', '2023-01-14', 2],
            ['2023-01-01', '2023-01-21', 3],
        ];
    }

    public static function numberOfWorkDaysProvider()
    {
        return [
            ['2023-11-21 8:00:00', '2023-11-21 16:00:00', 1],
            ['2023-11-21 8:00:00', '2023-11-26 16:00:00', 4],
            ['2023-11-23 8:00:00', '2023-11-24 16:00:00', 2],
            ['2023-11-25 8:00:00', '2023-11-26 16:00:00', 0],
            ['2023-11-21 8:00:00', '2023-11-29 16:00:00', 7],
            ['2023-11-26 8:00:00', '2023-11-30 16:00:00', 4],
            ['2023-11-27 8:00:00', '2023-11-28 08:00:00', 2],
        ];
    }

    public static function numberOfDaysProvider()
    {
        return [
            ['2023-11-21 8:00:00', '2023-11-21 16:00:00', 1],
            ['2023-11-21 8:00:00', '2023-11-26 16:00:00', 6],
            ['2023-11-23 8:00:00', '2023-11-24 16:00:00', 2],
            ['2023-11-25 8:00:00', '2023-11-26 16:00:00', 2],
            ['2023-11-21 8:00:00', '2023-11-29 16:00:00', 9],
            ['2023-11-26 8:00:00', '2023-11-30 16:00:00', 5],
            ['2023-11-27 8:00:00', '2023-11-28 08:00:00', 2],
        ];
    }

    public function testChecksValidTimeRange()
    {
        $this->expectException(\InvalidArgumentException::class);

        $from = new \DateTimeImmutable('2023-01-20');
        $to = new \DateTimeImmutable('2023-01-02');

        new DelegationTimeRange($from, $to);
    }


    #[DataProvider('numberOfWeeksProvider')]
    public function testCheckValidNumberOfWeeks(string $from, string $to, int $weeks)
    {
        $from = new \DateTimeImmutable($from);
        $to = new \DateTimeImmutable($to);

        $range = new DelegationTimeRange($from, $to);

        $this->assertEquals($weeks, $range->getNumberOfWeeks());
    }

    #[DataProvider('numberOfWorkDaysProvider')]
    public function testNumberOfDaysExcludingWeekends(string $from, string $to, int $workDays)
    {
        $from = new \DateTimeImmutable($from);
        $to = new \DateTimeImmutable($to);

        $range = new DelegationTimeRange($from, $to);

        $this->assertEquals($workDays, $range->getNumberOfDaysExcludingWeekend());
    }

    #[DataProvider('numberOfDaysProvider')]
    public function testNumberOfDays(string $from, string $to, int $workDays)
    {
        $from = new \DateTimeImmutable($from);
        $to = new \DateTimeImmutable($to);

        $range = new DelegationTimeRange($from, $to);

        $this->assertEquals($workDays, $range->getNumberOfDays());
    }
}
