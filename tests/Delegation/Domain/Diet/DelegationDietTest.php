<?php

namespace App\Tests\Delegation\Domain\Diet;

use App\Delegation\Domain\CountryEnum;
use App\Delegation\Domain\Diet\DelegationDiet;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class DelegationDietTest extends TestCase
{
    public static function delegationDietsProvider()
    {
        return [
            [CountryEnum::PL, 1, 10],
            [CountryEnum::PL, 2, 20],
            [CountryEnum::PL, 3, 30],
            [CountryEnum::PL, 4, 40],
            [CountryEnum::PL, 5, 50],
        ];
    }

    #[DataProvider('delegationDietsProvider')]
    public function testDietAmount(CountryEnum $countryAmount, int $factor, int $expectedAmount)
    {
        $diet = new DelegationDiet($countryAmount, $factor);

        $this->assertEquals($expectedAmount, $diet->getDietAmount());
    }

}
