<?php

namespace App\Tests\Delegation\Domain;

use App\Delegation\Domain\CountryEnum;
use App\Delegation\Domain\DelegationTimeRange;
use App\Delegation\Domain\Diet\DietCounter;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class DietCounterTest extends TestCase {

    public static function numberOfDaysWithDietProvider()
    {
        return [
            [
                CountryEnum::PL,
                '2023-11-21 8:00:00',
                '2023-11-21 16:00:00',
                CountryEnum::PL->amount(),
            ],
            [

                CountryEnum::PL,
                '2023-11-21 8:00:00',
                '2023-11-26 16:00:00',
                CountryEnum::PL->amount() * 4,

            ],
            [
                CountryEnum::PL,
                '2023-11-23 8:00:00',
                '2023-11-24 16:00:00',
                CountryEnum::PL->amount() * 2,
            ],
            [
                CountryEnum::PL,
                '2023-11-25 8:00:00',
                '2023-11-26 16:00:00',
                0,
            ],
            [
                CountryEnum::PL,
                '2023-11-21 8:00:00',
                '2023-12-01 16:00:00',
                (CountryEnum::PL->amount() * 5) + (CountryEnum::PL->amount() * 4 * 2),
            ],
            [
                CountryEnum::PL,
                '2023-11-21 23:00:00',
                '2023-12-01 03:00:00',
                (CountryEnum::PL->amount() * 6) + (CountryEnum::PL->amount() * 2),
            ],
            [
                CountryEnum::PL,
                '2023-11-21 08:00:00',
                '2023-12-08 16:00:00',
                (CountryEnum::PL->amount() * 5) + (CountryEnum::PL->amount() * 5 * 2) + (CountryEnum::PL->amount() * 4 * 3),
            ],
            [
                CountryEnum::PL,
                '2023-11-26 8:00:00',
                '2023-11-30 16:00:00',
                CountryEnum::PL->amount() * 4,
            ],
            [
                CountryEnum::PL,
                '2023-11-27 8:00:00',
                '2023-11-28 08:00:00',
                CountryEnum::PL->amount() * 2,
            ],
            [
                CountryEnum::PL,
                '2023-11-21 08:00:00',
                '2023-11-21 12:00:00',
                0,
            ],
            [
                CountryEnum::PL,
                '2023-11-21 08:00:00',
                '2023-11-22 03:00:00',
                CountryEnum::PL->amount(),
            ],
            [
                CountryEnum::PL,
                '2023-11-21 20:00:00',
                '2023-11-22 06:00:00',
                0,
            ],
            [
                CountryEnum::PL,
                '2023-11-24 23:00:00',
                '2023-11-26 15:00:00',
                0,
            ],
        ];
    }

    #[DataProvider('numberOfDaysWithDietProvider')]
    public function testAmountForDelegation(
        CountryEnum $country,
        string $fromDate,
        string $toDate,
        int $expectedAmount,
    ) {

        $timeRange = new DelegationTimeRange(
            new \DateTimeImmutable($fromDate),
            new \DateTimeImmutable($toDate),
        );
        $dietCounter = new DietCounter($timeRange, $country);
        $amount = $dietCounter->getAmountForDelegation();

        $this->assertEquals($expectedAmount, $amount);
    }
}
