# Delegation App

# Usage

```shell
$ docker-compose up -d # Build and run containers
$ docker-compose exec php composer install # Install Composer dependencies
$ docker-compose exec php bin/console doctrine:migrations:migrate # Run migrations
```
App will be available at `http://localhost:80`.

# API

## Create employer

Request
```http request
POST /employer/
```

Response
```json
{
  "id": "generated-employer-id"
}
```

## Create delegation for employer

Request
```http request
POST /delegation/
```
Body
```javascript
{
    "employerId": string,
    "country": enum,
    "endDate": string,
    "startDate": string
}
```
- `employerId` - UUID value of employer
- `country` - `PL`, `DE` or `GB`
- `startDate` - Start datetime of delegation (ISO-8601)
- `endDate` - End datetime of delegation (ISO-8601)

Response:
```javascript
{
    "id": string
}
```

## Get employer's delegations

Request:
```http request
GET /delegation/employer/{employerId}
```

Response:
```javascript
{
    "delegations": [
        {
            "start": string,
            "end": string,
            "country": string,
            "amount_due": number,
            "currency": string
        },
        ...
    ]
}
```

